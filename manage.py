#!/bin/env python
from flask_migrate import Migrate, MigrateCommand
from flask_script import Server, Manager

# Setup the manager/migration commands
from app import app
from models import db

manager = Manager(app)
migrate = Migrate(app, db)

# Create a server object
server = Server(host='0.0.0.0', port=5005, use_debugger=True, use_reloader=True)

# Add commands to the manager
manager.add_command('db', MigrateCommand)
manager.add_command("run", server)

# run!
if __name__ == '__main__':
    manager.run()
