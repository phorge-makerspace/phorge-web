from flask import Blueprint, jsonify

from helpers import Calendar, DBConnection
from models import Post, Event

data = Blueprint("Data", __name__)


@data.route('/data/events/<int:max_events>')
@data.route('/data/events', defaults={'max_events': 10})
def event_data(max_events: int):
    calendar = Calendar()

    with DBConnection() as db:
        events = calendar.get_json_events(db, max_events)

    return jsonify(events)


@data.route('/data/blogs/<int:limit>')
@data.route('/data/blogs', defaults={'limit': 100})
def blogs_data(limit: int):
    with DBConnection() as db:
        posts = db.session.query(Post).limit(limit).all()

    return jsonify(posts)
