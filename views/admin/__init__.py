from flask import render_template, Blueprint, url_for
from flask_login import login_required

from helpers import DBConnection
from models import Post, User

admin = Blueprint("Admin", __name__)


@admin.route("/admin")
@login_required
def dashboard():
    return "Admin page"


@admin.route("/admin/users")
@login_required
def users():
    return "Admin page"


@admin.route("/admin/users/add")
@login_required
def users_add():
    return "Admin page"


@admin.route("/admin/users/delete/<user_id>")
@login_required
def users_delete(user_id):
    return "Admin page"
