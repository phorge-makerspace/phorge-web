from datetime import datetime

from flask import render_template, Blueprint, url_for, request
from flask_login import login_required, current_user

from helpers import DBConnection
from models import Post
from markdownify import markdownify as md


blog = Blueprint("Blog", __name__)


@blog.route("/blog/")
@blog.route("/blog")
def home():
    with DBConnection() as db:
        blog_posts = db.session.query(Post).all()

    print(url_for("Blog.blog_post", title="Test"))
    return render_template(
        "blog/blog_home.html",
        blog_posts=blog_posts
    )


@blog.route("/blog/<title>/")
@blog.route("/blog/<title>")
def blog_post(title):

    with DBConnection() as db:
        post = db.session.query(Post).filter(
            Post.title == title
        ).first()

    return render_template(
        "blog/blog.html",
        post=post
    )


@blog.route("/blog/<title>/edit/")
@blog.route("/blog/<title>/edit")
@login_required
def blog_edit(title):

    with DBConnection() as db:
        post = db.session.query(Post).filter(
            Post.title == title
        ).first()

    return render_template(
        "blog/blog_editor.html",
        post=post
    )


@blog.route("/blog/save/", methods=["POST"])
@blog.route("/blog/save", methods=["POST"])
def blog_save():
    data = request.form
    title = "Blog post"
    content = ""

    # It's easier to ask for forgiveness than permission, check if we can look up the post, otherwise fail to None
    # noinspection PyBroadException
    try:
        with DBConnection() as db:
            existing_post = db.session.query(Post).filter(Post.post_id == data["post_id"]).first()
    except:
        existing_post = None

    if "title" in data.keys():
        # remove the nonsense from the title
        title = data["title"]
        title = title.replace("<p>", "").replace("</p>", "")
        title = title.replace("<h1>", "").replace("</h1>", "")
        title = title.replace("\n", "").replace("\r", "").replace("  ", "")

    if "content" in data.keys():
        content = md(data["content"])

    with DBConnection() as db:
        if not existing_post:
            post = Post(title, content, user_id=current_user.user_id)
            db.session.add(post)
            db.session.commit()
        else:
            if title != "" and title != existing_post.title:
                existing_post.title = title

            existing_post.content = content
            if existing_post.created_timestamp is None:
                existing_post.created_timestamp = datetime.now()

            existing_post.updated_timestamp = datetime.now()

            # add it back to the db
            db.session.add(existing_post)
            db.session.commit()
            # set title, not sure this is necessary...
            title = existing_post.title

    return url_for("Blog.blog_post", title=title)


@blog.route("/blog/new/")
@blog.route("/blog/new")
@login_required
def blog_new():
    return render_template(
        "blog/blog_editor.html",
    )
