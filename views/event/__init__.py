from datetime import datetime

from flask import render_template, redirect, Blueprint, url_for, request, current_app
from flask_login import login_required, current_user
from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Email

from helpers import DBConnection, MailHelper
from models import Event, EventRegistration
from markdownify import markdownify as md

import uuid

event = Blueprint("Event", __name__)

class EventRegForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])

@event.route("/event/<id>/")
def home(id, form=None):
    with DBConnection() as db:
        event = db.session.query(Event).get(id)

        allregs = db.session.query(EventRegistration) \
            .filter(EventRegistration.event_id == id)

        regs = allregs \
            .filter(EventRegistration.email_confirmed == True) \
            .all()

        allregs = allregs.all()

    if form is None:
        form = EventRegForm()

    return render_template(
        "event/event.html",
        event=event,
        regs=regs,
        allregs=allregs,
        form=form,
        user=current_user
    )

@event.route('/event/reg/<id>/', methods=('POST', 'GET'))
def register(id):
    if request.method == 'GET':
        return redirect( url_for('Event.home', id=id) )

    form = EventRegForm()

    if not form.validate_on_submit():
        return home(id, form)

    # Check for the following scenarios:
    #   1. Email is already registered (or awaiting confirmation) for this event
    #   2. Registration limit already reached (this would only occur if someone 
    #      registered the last spot while this person was registering, or they
    #      bypassed and submitted the data manually for a full event)
    with DBConnection() as db:
        event = db.session.query(Event).get(id)
        regs = db.session.query(EventRegistration) \
            .filter(EventRegistration.event_id == id)

        # Registration limit reached
        if regs.count() >= event.registration_limit:
            return redirect( url_for('Event.home', id=id) )

        reg = regs.filter(EventRegistration.email == form.email.data).first()

        if reg is not None:
            return render_template(
                "event/reg/duplicate.html",
                reg=reg)

    reg = EventRegistration()

    reg.event_id = id
    reg.first_name = form.first_name.data
    reg.last_name= form.last_name.data
    reg.email = form.email.data
    reg.email_confirmed = False
    reg.confirmation_token = uuid.uuid4()

    with DBConnection() as db:
       db.session.add(reg)
       db.session.commit()
       db.session.refresh(reg)

    MailHelper.send_template("Phorge Event Registration", reg.email,
        "mail/event_reg_confirmation.html",
        {"event": event, "reg": reg, "confirmation_link": url_for('Event.confirm', token=reg.confirmation_token, _external=True)})

    # Add email to mailchimp lists
    try:
        current_app.mailchimp.add_to_list(reg.email, reg.first_name, reg.last_name)
        current_app.mailchimp.add_to_list(reg.email, reg.first_name, reg.last_name, event.maillist)
    except:
        # Just move on if mailchimp reg failed, we don't want to send a 500 for this
        # is there a log file or something we could log the error to?
        pass

    return render_template(
        "event/reg/init.html",
        reg=reg
    )

@event.route('/event/confirm/<token>/')
def confirm(token):
    # Search for a registration entry with a matching token
    event = None
    with DBConnection() as db:
        reg = db.session.query(EventRegistration) \
            .filter(EventRegistration.confirmation_token == token) \
            .first()

        if reg:
            if not reg.email_confirmed:
                reg.email_confirmed = True
                db.session.commit()

            event = db.session.query(Event).get(reg.event_id)

    if reg is None:
        return render_template(
            "event/reg/notfound.html")

    return render_template(
        "event/reg/complete.html",
        event=event,
        reg=reg
    )
