from datetime import datetime

import flask
from flask import render_template, Blueprint, request, url_for, redirect
from flask_admin.helpers import is_safe_url
from flask_login import logout_user, current_user, login_user

from forms import LoginForm
from helpers import DBConnection
from helpers.security import password_hash
from models import User
from .blog import blog
from .data import data
from .event import event

main = Blueprint("Main", __name__)


@main.route('/')
def home():
    return render_template('home.html')


@main.route('/login', methods=["GET", "POST"])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for("Main.home"))

    # create an instance of the LoginForm
    login_form = LoginForm()

    if login_form.validate_on_submit():
        username = login_form.data["username"]
        password = login_form.data["password"]

        auth = False
        logged_in = False

        with DBConnection() as db:
            user = db.session.query(User).filter(User.username == username).first()

        if user and user.password == password_hash(password, user.password_salt):
            auth = True

        if auth:
            try:
                logged_in = login_user(user)
            except:
                # don't really care if errors happen here
                pass

        if logged_in:
            user.authenticated = True
            user.last_login = datetime.now()
            with DBConnection() as db:
                db.session.add(user)
                db.session.commit()
                db.session.close()

            next_url = request.args.get("next")

            if next_url is None or not is_safe_url(next_url):
                return redirect(url_for("Main.home"))
            else:
                return redirect(next_url or url_for("Main.home"))

    return render_template(
        "login.html",
        login_form=login_form
    )


@main.route('/logout')
def logout():
    user = current_user
    if user.is_authenticated:
        logout_user()
    return redirect(url_for("Main.home"))
