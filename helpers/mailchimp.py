import mailchimp3

class MailChimpHelper(object):
    MASTER_LIST_NAME = 'Phorge Makerspace'

    def __init__(self, apikey):
        self._api = mailchimp3.MailChimp(mc_api=apikey)

    def add_to_list(self, email, first_name, last_name, list_name=None):
        if list_name is None:
            list_name = MailChimpHelper.MASTER_LIST_NAME

        # check if the list exists
        lists = [l for l in self._api.lists.all()['lists'] if l['name'] == list_name]

        if len(lists) == 0: # create the list
            list_data = {
                'name': list_name,
                'contact': {
                    'company': 'Phorge Makerspace',
                    'address1': '241 N Main St',
                    'city': 'Sheridan',
                    'state': 'WY',
                    'zip': '82801',
                    'country': 'US'
                },
                'permission_reminder': 'You are receiving this email because you signed up for a Phorge Makerspace event in the past',
                'campaign_defaults': {
                    'from_name': 'Phorge',
                    'from_email': 'mail@phorge.co',
                    'subject': 'Phorge Makerspace',
                    'language': 'en-us'
                },
                'email_type_option': False
            }
            tlist = self._api.lists.create(data=list_data)
        else:
            tlist = lists[0]

        # Now add the info to the target list
        user_data = {
            'email_address': email,
            'status': 'subscribed',
            'merge_fields': {
                'FNAME': first_name,
                'LNAME': last_name
            }
        }

        try:
            self._api.lists.members.create(list_id=tlist['id'], data=user_data)
        except mailchimp3.mailchimpclient.MailChimpError as mcerr:
            # ignore error if it's because member is already in list, otherwise re-raise it
            if mcerr.args[0]['title'] != 'Member Exists':
                raise mcerr

