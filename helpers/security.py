from bcrypt import gensalt, hashpw


def create_salt():
    """
    Creates cryptographic salt
    :return string: salt
    """

    return str(gensalt(), 'utf-8')


def password_hash(password, salt):
    """
    Creates cryptographic hash of a password
    :param string password: plaintext password
    :param string salt: crypto salt
    :return string: hashed password
    """

    password_bytes = password + salt

    # For some reason hashpw needs things to be encoded specifically as utf-8, this is dumb, but it works
    return str(hashpw(password_bytes.encode('utf-8'), salt.encode('utf-8')), 'utf-8')
