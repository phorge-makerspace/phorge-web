import datetime
import iso8601

from googleapiclient.discovery import build
from config import GOOGLE_API_KEY

from models.Event import Event as PhorgeEvent

class Event:
    """
    Event object holds very basic info about events
    """

    def __init__(self, summary="", start_time=None, end_time=None, link=None, google_id=None, phorge_id=None):
        """
        Create a new Event Object
        :param str summary: Summary of the event
        :param datetime start_time: Start time of the event
        :param datetime end_time: End time of the event
        :param str link: Link to google event
        :param str google_id: Unique id for event in Google Calendar
        :param int phorge_id: Unique id for event in Phorge database
        """
        self.summary = summary
        self._start_time = start_time
        self._end_time = end_time
        self.link = link
        self.google_id = google_id
        self.phorge_id = phorge_id

    def __repr__(self):
        # return a nice string represenation of the object
        return f"{self.summary} - {self.start_time}"

    def __str__(self):
        # return a nice string represenation of the object
        return f"{self.summary} - {self.start_time}"

    def to_dict(self):
        return {
            "summary": self.summary,
            "date": self.start_time,
            "link": self.link,
            "google_id": self.google_id,
            "phorge_id": self.phorge_id
        }

    @property
    def start_time(self):
        # prettify the start_time
        return self._start_time.strftime('%B %d, %Y %H:%M')

    @property
    def end_time(self):
        # prettify the end_time
        return self._start_time.strftime('%B %d, %Y %H:%M')


class Calendar:
    def __init__(self):
        self.cal_id = "ross49ippfj8vj4mi5hliprcqc@group.calendar.google.com"
        self.service = build('calendar', 'v3', developerKey=GOOGLE_API_KEY)
        self.max_events = 10

    def get_events(self, db, max=None):
        if not max:
            max = self.max_events

        events_result = self.service.events().list(
            calendarId=self.cal_id,
            timeMin=f"{datetime.datetime.utcnow().isoformat()}Z",
            maxResults=max,
            singleEvents=True,
            orderBy='startTime'
        ).execute()

        events_dict = events_result.get('items', [])
        output = []
        for event in events_dict:
            start_time = iso8601.parse_date(event["start"]["dateTime"])
            end_time = iso8601.parse_date(event["end"]["dateTime"])
            output.append(Event(event["summary"], start_time, end_time, event["htmlLink"], google_id=event["id"]))
        
        # Search our database for events with references to the google calendar id
        for event in output:
            phorge_event = db.session.query(PhorgeEvent).filter(PhorgeEvent.google_id == event.google_id).one_or_none()
            if phorge_event:
                event.phorge_id = phorge_event.event_id

        return output

    def get_json_events(self, db, max=None):
        events = self.get_events(db, max)
        output = []

        for event in events:
            output.append(event.to_dict())

        return output


# this is bad, just for testing
if __name__ == "__main__":
    calendar = Calendar()
    events = calendar.get_events()
    for event in events:
        print(event)
