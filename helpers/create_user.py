#!/bin/env python
import traceback
from getpass import getpass

import click
from flask.cli import AppGroup

from helpers import DBConnection
from models import User

# define the user_cli so
user_cli = AppGroup("user")


# noinspection PyBroadException
def create_user(first_name, last_name, email, password, admin=False):
    with DBConnection() as db:
        existing_user = db.session.query(User).filter(User.email == email).first()

    if existing_user:
        print(f"User with email:{email} already exists")
        return None

    try:
        with DBConnection() as db:
            new_user = User(
                username=email,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                admin=admin
            )
            db.session.add(new_user)
            db.session.commit()

            print(f"Created user {new_user.user_id}")
            return new_user.user_id
    except Exception as ex:
        print(f"Failed to create user {first_name}{last_name} due to {ex}\n {traceback.format_exc()}")
        return None


@user_cli.command("create")
@click.argument("email", required=False)
@click.argument("first_name", required=False)
@click.argument("last_name", required=False)
def interactively_create_user(email=None, first_name=None, last_name=None):
    if not email:
        email = input("E-mail address of new user: \n")
    if not first_name:
        first_name = input("First name: \n")
    if not last_name:
        last_name = input("Last name: \n")
    password = getpass("password: \n")

    create_user(first_name, last_name, email, password, admin=True)


if __name__ == "__main__":
    interactively_create_user()
