from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import SQLALCHEMY_DATABASE_URI


class DBConnection(object):
    """
    Similar to how create_db_session is used, except can be done in a with: block
    The real advantage of this is that it will automatically clean up after itself
    This eliminates some of the issues we were having with too many connections
    example:
    ```
    with DBConnection() as db:
        users = db.session.query(User).all()
    ```
    """

    def __init__(self, constring=""):
        """
        Create a new instance of the DBConnection Object, for use during a with:
        :param string constring: A specific connection string (default "")
        """
        self.constring = constring
        self.session = None

    def __enter__(self):
        """
        Sets up the entry point of the session
        :return: self
        """
        # Setup the connection string if one's not been provided
        if self.constring == "":
            self.constring = SQLALCHEMY_DATABASE_URI

        # Setup the engine to use
        self.__engine = create_engine(self.constring)

        # Create a new SessionMaker
        session_maker = sessionmaker(bind=self.__engine)

        # create a session with session_maker()
        self.session = session_maker()

        # Return this whole object for use
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Commands to run after with:block has ended
        :param exc_type: NA
        :param exc_val: NA
        :param exc_tb: NA
        :return:
        """
        # Close session
        self.session.close()
        # Dispose of the engine
        self.__engine.dispose()
