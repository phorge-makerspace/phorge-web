from flask import render_template
from flask_mail import Message

class MailHelper(object):
    MAIL = None # Static variable that is set in app.py

    @staticmethod
    def send_test(recipient):
        msg = Message("Phorge Mail Test", recipients=[recipient])
        msg.body = "This is a test"
        msg.html = "<p style=\"color:blue\">This is an HTML test</p>"

        MailHelper.MAIL.send(msg)

    @staticmethod
    def send_template(subject, recipient, template, template_vars, plain_text=None):
        msg = Message(subject, recipients=[recipient])
        if plain_text:
            msg.body = plain_text
        else:
            msg.body = "This message is best viewed from a mail client that can view HTML"

        msg.html = render_template(template, **template_vars)

        MailHelper.MAIL.send(msg)

