from pathlib import Path
from dotenv import load_dotenv

import os


# load env file ########################################################################################################
env_path = Path(".") / ".env"

if not os.path.isfile(env_path):
    env_path = Path(".") / "phorge.dev.env"

load_dotenv(dotenv_path=env_path)

DEBUG = bool(os.getenv("DEBUG"))

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_ECHO = bool(os.getenv("ECHO"))
SQLALCHEMY_TRACK_MODIFICATIONS = bool(os.getenv("TRACK"))
SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@{}/{}".format(
    os.getenv("POSTGRES_USER"),
    os.getenv("POSTGRES_PASS"),
    os.getenv("POSTGRES_HOST"),
    os.getenv("POSTGRES_DB")
)
DATABASE_QUERY_TIMEOUT = 30

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY")

SECRET_KEY = os.getenv("SECRET_KEY")

MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = os.getenv("MAIL_USERNAME")
MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
MAIL_DEFAULT_SENDER = ("Phorge Makerspace", MAIL_USERNAME)

MAILCHIMP_API_KEY = os.getenv("MAILCHIMP_API_KEY")

