from flask import Flask
from flask_login import LoginManager, current_user
from flask_migrate import Migrate
from flask_mail import Mail, Message
from flask_admin import Admin as FAdmin
from flask_admin.contrib.sqla import ModelView as FModelView
# from flask_sqlalchemy import SQLAlchemy
from models import db, Post, User, Event, EventRegistration
from helpers import MailHelper, MailChimpHelper

# load blueprints  ############################################
from helpers.create_user import user_cli


def load_blueprints():
    from views import blog, main, data, event

    app.register_blueprint(data)
    app.register_blueprint(blog)
    app.register_blueprint(main)
    app.register_blueprint(event)


# Application setup ###########################################
app = Flask(__name__)
app.config.from_pyfile("config.py")

# Database Setup ##############################################
db.init_app(app)
migrate = Migrate(app, db)

# User arguments ##############################################
app.cli.add_command(user_cli)

# Login manager ###############################################
login_manager = LoginManager()
login_manager.init_app(app)

# Mail setup ##################################################
MailHelper.MAIL = Mail(app)

# MailChimp API setup #########################################
app.mailchimp = MailChimpHelper(app.config['MAILCHIMP_API_KEY'])

# Admin interface setup #######################################
class ModelView(FModelView):
    column_exclude_list = ['password', 'password_salt']
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_admin

admin = FAdmin(app, name="phorge", template_mode="bootstrap3")
admin.add_view(ModelView(Post, db.session))
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Event, db.session))
admin.add_view(ModelView(EventRegistration, db.session))

@login_manager.user_loader
def load_user(user_id):
    """
    This is needed by the login manager to lookup a user from an user ID
    :param user_id:
    :return:
    """
    # since there could be issues with cyclic loading, load these locally
    from helpers import DBConnection
    from models import User

    with DBConnection() as db_conn:
        user = db_conn.session.query(User).filter(User.user_id == user_id).first()

    return user


# Load blueprints #############################################
load_blueprints()
