
PHORGE Makerspace website
====


Environment Setup instructions
----
1. Create a python3.6 virtual environment inside the directory
   1. `virtualenv -p python3.6 venv`
2. Activate and install requirements
   1. `source venv/bin/activate`
   2. `pip install -r requirements.txt`
3. Modify environment file
   1. If you don't already have a .env file, create one by copying the phorge.dev.env config: `cp phorge.dev.env .env`
   2. Modify the `.env` file to match your local configuration
4. Upgrade the database
   1. run `flask db upgrade`
5. Create user (if fresh DB)
   1. Run `flask user create`
   2. Follow the instructions to make the user
6. Run application
   1. `flask run`

Database migration
----
When changing any model definition to get the database schema up to date, you have to generate a migration and apply it

1. Generate migration
   1. `flask db migrate`
   2. *Note: it may be necessary to manually modify the migration*
   3. *Migrations are stored under migrations/versions*
2. Apply migration
   1. `flask db upgrade`
    