from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .Post import Post
from .User import User
from .Event import Event, EventRegistration
