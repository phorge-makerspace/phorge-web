import markdown
from . import db
from datetime import datetime
from flask import Markup


class Event(db.Model):
    __tablename__ = "event"

    event_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    description = db.Column(db.Text)
    when = db.Column(db.DateTime)
    where = db.Column(db.String)
    registration_limit = db.Column(db.Integer, default=20)
    visible = db.Column(db.Boolean, default=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.user_id"))
    google_id = db.Column(db.String, index=True)

    user = db.relationship("User", uselist=False, lazy="subquery")

    def __str__(self):
        return self.title

    @property
    def summary(self):
        limit = 150

        return self.html_content[:limit]

    @property
    def html_content(self):
        html_content = Markup(markdown.markdown(self.content))
        return html_content

    @property
    def friendly_timestamp(self):
        ftime = "%A, %B %-d, %Y  %-I:%M %p"
        return datetime.strftime(self.when, ftime)

    @property
    def expired(self):
        return datetime.now() >= self.when

    @property
    def maillist(self):
        return f"Event {self.event_id} - {self.title}"

"""
    A person's registration information for an event.
"""
class EventRegistration(db.Model):
    __tablename__ = "eventreg"

    eventreg_id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey("event.event_id"))
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    email = db.Column(db.String)
    phone = db.Column(db.String)

    email_confirmed = db.Column(db.Boolean, default=False)
    confirmation_token = db.Column(db.String)

    event = db.relationship("Event", uselist=False, lazy="subquery")

    def __init__(self):
        pass

