import markdown
from . import db
from datetime import datetime
from flask import Markup


class Post(db.Model):
    __tablename__ = "post"

    post_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(), unique=True, nullable=False)
    content = db.Column(db.Text())
    published = db.Column(db.Boolean(), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.user_id"))
    created_timestamp = db.Column(db.DateTime(), index=True)
    updated_timestamp = db.Column(db.DateTime(), index=True)

    user = db.relationship("User", uselist=False, lazy='subquery')

    def __init__(self, title, content, user_id):
        self.title = title
        self.content = content
        self.published = True
        self.created_timestamp = datetime.now()
        self.user_id = user_id

    @property
    def summary(self):
        limit = 150

        return self.html_content[:limit]

    @property
    def html_content(self):
        html_content = Markup(markdown.markdown(self.content))
        return html_content

    @property
    def friendly_timestamp(self):
        ftime = "%A %B %d, %Y  %H:%M %p"
        return datetime.strftime(self.created_timestamp, ftime)
