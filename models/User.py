from datetime import datetime

from helpers.security import password_hash, create_salt
from . import db


class User(db.Model):
    __tablename__ = "user"

    user_id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    username = db.Column(db.String(64), unique=True)
    password = db.Column(db.String)
    password_salt = db.Column(db.String)
    description = db.Column(db.String)
    authenticated = db.Column(db.Boolean)
    is_admin = db.Column(db.Boolean, default=False)
    active = db.Column(db.Boolean)
    email = db.Column(db.String, unique=True)
    phone = db.Column(db.String)
    created = db.Column(db.DateTime)
    updated = db.Column(db.DateTime)

    def __init__(self, username='', first_name='', last_name='', email='', password='', admin=False):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

        # create salt, then hash the password with it
        self.set_salt()
        self.set_password(password)

        self.authenticated = False
        self.active = True
        self.is_admin = admin

        self.created = datetime.now()
        self.updated = datetime.now()

    def __repr__(self):
        return f"{self.first_name} {self.last_name}"

    def set_password(self, password):
        self.password = password_hash(password, self.password_salt)

    def set_salt(self):
        self.password_salt = create_salt()

    @property
    def is_authenticated(self):
        return self.authenticated

    @is_authenticated.setter
    def is_authenticated(self, value):
        self.authenticated = value

    @property
    def is_active(self):
        return self.active

    @is_active.setter
    def is_active(self, value):
        self.active = value

    def get_id(self):
        return self.user_id

    @property
    def is_anonymous(self):
        return False
